package lk.jenkinskt.demo.service.serviceImpl;

import lk.jenkinskt.demo.dto.request.UserLoginDetails;
import lk.jenkinskt.demo.dto.response.UserLoginResponse;
import lk.jenkinskt.demo.service.UserLoginService;
import org.springframework.stereotype.Service;

@Service
public class UserLoginServiceImpl implements UserLoginService {
    @Override
    public UserLoginResponse userLoginHandler(UserLoginDetails userLoginDetails) {
        return new UserLoginResponse(
                userLoginDetails.getUsername(),
                "TEST_SUCCESS_CODE_200",
                "User login success"
        );
    }
}
