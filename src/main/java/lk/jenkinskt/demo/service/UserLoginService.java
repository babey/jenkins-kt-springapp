package lk.jenkinskt.demo.service;

import lk.jenkinskt.demo.dto.request.UserLoginDetails;
import lk.jenkinskt.demo.dto.response.UserLoginResponse;

public interface UserLoginService {
    UserLoginResponse userLoginHandler(UserLoginDetails userLoginDetails);
}
