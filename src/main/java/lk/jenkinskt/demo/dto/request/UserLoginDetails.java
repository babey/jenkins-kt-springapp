package lk.jenkinskt.demo.dto.request;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UserLoginDetails {
    private String username;
    private String password;
}
