package lk.jenkinskt.demo.dto.response;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UserLoginResponse {

    private String username;
    private String responseCode;
    private String details;

}
