package lk.jenkinskt.demo.controller;

import lk.jenkinskt.demo.dto.request.UserLoginDetails;
import lk.jenkinskt.demo.service.UserLoginService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/user")
public class UserController {

    private final UserLoginService userLoginService;

    public UserController(UserLoginService userLoginService) {
        this.userLoginService = userLoginService;
    }

    @PostMapping(path = "/login")
    public ResponseEntity<Object> userLogin(@RequestBody UserLoginDetails userLoginDetails) {
        return  ResponseEntity.ok().body(
            userLoginService.userLoginHandler(userLoginDetails)
        );
    }
}
